== README

## Project Setup Instructions

```
cd todo
bundle install
```
Now create `database.yml` file, and copy paste the content from `database.yml` file.

```
rake db:create
rake db:migrate
