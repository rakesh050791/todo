Rails.application.routes.draw do

  #root 'tasks#index'

  resources :tasks

  devise_for :users
   devise_scope :user do
    authenticated :user do      
       root :to => 'tasks#index'
    end

    unauthenticated :user do
      root :to => 'devise/registrations#new', as: :unauthenticated_root
    end
  end

  get 'mark_complete', :to => 'tasks#mark_complete'

end
