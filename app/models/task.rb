class Task < ActiveRecord::Base
  belongs_to :user
  validates :title,:description, presence: true

  def self.search params
    Task.where("tasks.title LIKE :term OR tasks.description LIKE :term", :term => "%#{params}%" ) 
  end
end
