// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require select2
//= require_tree .

$(document).ready(function() {

//===========Select 2 ================
    $("#task_assignee_id").select2({
      allowClear: true,     
      width: "100%"
    });

//===========End =====================

//===============Ajax for search=========
    $("#search-submit").on("click", function () {  
      var value =  $("#q").val();
      $.ajax({
          type: "GET",
          url: "/",
          data:{ q: value  },
          dataType: "script",
          success:function(data){               
          }
        }); 
  });

  //=======================End===========================================  

  //===============Ajax for MARK COMPLETE=========
    $(".rght").on("click", function () {  
      var value =  $(".rght").val();
      alert("Are you sure you want to mark this task as complete?");
      //window.location.reload();
      $.ajax({
          type: "GET",
          url: "/mark_complete",
          data:{ id: value  },
          dataType: "script",
          success:function(data){               
          }
        }); 
      	window.location.reload();
  });

  //=======================End===========================================  

});