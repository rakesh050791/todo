class TasksController < ApplicationController
  before_filter :auth_user
  before_action :set_task, only: [:show, :edit, :update, :destroy,:mark_complete]
  
  # GET /tasks
  # GET /tasks.json
  def index
    @task = Task.new
    if params[:q]
      @tasks = Task.search(params[:q]).paginate(page: params[:page], per_page: 5)
    else
      @tasks = Task.where("user_id = ? or assignee_id = ?", current_user.id, current_user.id).paginate(page: params[:page], per_page: 5).order('created_at DESC')
    end
    respond_to do |format|
      format.html # index.html.erb 
      format.js   # index.js.erb  
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = current_user.tasks.build(task_params)

     respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path, notice: 'Task created successfully.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to @task, notice: 'Task updated successfully.' }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def mark_complete
      @task.update(is_complete: true)
      respond_to do |format|
      if @task
        format.html { redirect_to tasks_path, notice: 'Task Updated.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:title, :description, :user_id, :assignee_id, :is_complete)
    end
end
